package me.tomasalmeida.courses.listeners.ui;

import me.tomasalmeida.courses.helpers.PlayerHelper;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class KitMenu implements Listener {

    private Inventory inventory;

    public KitMenu(){
        inventory = Bukkit.createInventory(null, 9*3);
    }

    @EventHandler
    public void onClickItem(PlayerInteractEvent event){
        Player player = event.getPlayer();
        ItemStack itemHand = player.getItemInHand();

        String kitItemName = PlayerHelper.JOIN_ITEM_KITS.getItemMeta().getDisplayName();

        if(itemHand == null){
            return;
        }else if(itemHand.getItemMeta() != null){
            return;
        }else if(!itemHand.getItemMeta().hasDisplayName()){
            return;
        }else if(!itemHand.getItemMeta().getDisplayName().equalsIgnoreCase(kitItemName)){
            return;
        }


    }
}

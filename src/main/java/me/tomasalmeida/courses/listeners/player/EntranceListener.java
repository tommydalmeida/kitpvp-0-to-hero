package me.tomasalmeida.courses.listeners.player;

import me.tomasalmeida.courses.helpers.PlayerHelper;
import me.tomasalmeida.courses.user.UserManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class EntranceListener implements Listener {

    private UserManager userManager;
    private PlayerHelper playerHelper;

    public EntranceListener(){
        userManager = UserManager.getInstance();
        playerHelper = PlayerHelper.getInstance();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();

        userManager.add(player);
        playerHelper.clear(player);
        playerHelper.giveJoinItems(player);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event){
        Player player = event.getPlayer();

        userManager.save(player);
    }
}

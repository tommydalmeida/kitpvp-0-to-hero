package me.tomasalmeida.courses.helpers;

import me.tomasalmeida.courses.utils.Consts;
import me.tomasalmeida.courses.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

public class PlayerHelper {

    private static PlayerHelper instance;

    public static final ItemStack JOIN_ITEM_KITS = new ItemBuilder(Material.BLAZE_POWDER)
            .title(Consts.JOIN_KIT_ITEM_NAME)
            .build();

    private PlayerHelper(){
    }

    public void clear(Player player){
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(20);
        player.setExhaustion(0f);
        player.setFallDistance(0f);
        player.setFireTicks(0);
        player.setAllowFlight(false);

        for (PotionEffect pE : player.getActivePotionEffects()) {
            player.removePotionEffect(pE.getType());
        }
    }
    public void giveJoinItems(Player player){
        player.getInventory().setItem(4, JOIN_ITEM_KITS);
    }

    public static PlayerHelper getInstance(){
        if(instance == null){
            instance = new PlayerHelper();
        }

        return instance;
    }
}

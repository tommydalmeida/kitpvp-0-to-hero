package me.tomasalmeida.courses.user;

import me.tomasalmeida.courses.kits.Kit;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity(value = "users", noClassnameStored = true)
public class User {

    @Id
    private ObjectId id;

    private String uuid;
    private List<Kit> kits;

    public User(){
        kits = new ArrayList<>();
    }

    public User(UUID uuid){
        this();
        this.uuid = uuid.toString();
    }

    public ObjectId getId() {
        return id;
    }

    public void add(Kit kit){
        kits.add(kit);
    }
}

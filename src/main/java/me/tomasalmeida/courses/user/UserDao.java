package me.tomasalmeida.courses.user;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class UserDao extends BasicDAO<User, ObjectId> {

    public UserDao(Class<User> entityClass, Datastore ds) {
        super(entityClass, ds);
    }


}

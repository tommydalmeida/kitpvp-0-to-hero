package me.tomasalmeida.courses.user;

import org.bukkit.entity.Player;

import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;

public class UserManager {

    private static UserManager instance;

    private Map<UUID, User> cache;
    private UserService userService;

    private UserManager(){
        cache = new WeakHashMap<>();
        userService = new UserServiceImpl();
    }

    /**
     * Cache's the user
     * @param player player
     */
    public void add(Player player){
        if(hasUser(player)){
            return;
        }

        UUID uuid = player.getUniqueId();
        User user = new User(uuid);

        cache.put(uuid, user);
    }

    /**
     * Removes the player from cache
     * @param player
     */
    public void remove(Player player){
        if(!hasUser(player)){
            return;
        }

        UUID uuid = player.getUniqueId();
        cache.remove(uuid);
    }

    /**
     * Saves the player to the database
     * @param player player
     */
    public void save(Player player){
        UUID uuid = player.getUniqueId();
        User user = cache.get(uuid);

        if(user == null){
            return;
        }

        userService.save(user);
    }

    /**
     * Saves every users cached into the database
     */
    public void saveAll(){
        cache.forEach((uuid, user) -> userService.save(user));
    }

    private boolean hasUser(Player player) {
        return cache.containsKey(player.getUniqueId());
    }

    public static UserManager getInstance(){
        if(instance == null){
            instance = new UserManager();
        }

        return instance;
    }
}

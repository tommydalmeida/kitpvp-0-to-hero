package me.tomasalmeida.courses.user;

import me.tomasalmeida.courses.database.Daos;

public class UserServiceImpl implements UserService {

    private UserDao userDao;

    public UserServiceImpl() {
        userDao = Daos.getInstance().getUserDao();
    }

    @Override
    public void save(User user) {
        userDao.save(user);
    }
}

package me.tomasalmeida.courses.kits;

import org.bukkit.entity.Player;

import java.util.*;

public class KitManager {

    private static KitManager instance;

    private Map<Player, Kit> players;
    private Set<Kit> kits;

    private KitManager(){
        players = new HashMap<>();
        kits = new HashSet<>();
    }

    public void add(Kit kit){
        kits.add(kit);
    }

    public void apply(Player player, Kit kit){
        if(!player.hasPermission(kit.getPermission())){
            //TODO: send information to player
            return;
        }

        if(hasKit(player)){
            //TODO: send information to player
            return;
        }

        players.put(player, kit);
        kit.apply(player);
    }

    private boolean hasKit(Player player) {
        return players.containsKey(player);
    }

    public static KitManager getInstance(){
        if(instance == null){
            instance = new KitManager();
        }

        return instance;
    }
}

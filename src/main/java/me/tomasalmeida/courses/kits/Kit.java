package me.tomasalmeida.courses.kits;

import org.bukkit.entity.Player;
import org.mongodb.morphia.annotations.Transient;

public abstract class Kit {

    private String name;
    private String[] description;

    @Transient
    private String permission;

    public Kit(String name, String... description){
        this.name = name;
        this.description = description;
        this.permission = "kit." + name;
    }

    public abstract void apply(Player player);

    public String getPermission() {
        return permission;
    }
}

package me.tomasalmeida.courses.database;

import me.tomasalmeida.courses.user.UserDao;

public class Daos {

    private Database database;

    private static Daos instance;

    private Daos(){
        database = Database.getInstance();
    }

    public UserDao getUserDao(){
        return database.getUserDao();
    }

    public static Daos getInstance(){
        if(instance == null){
            instance = new Daos();
        }

        return instance;
    }
}

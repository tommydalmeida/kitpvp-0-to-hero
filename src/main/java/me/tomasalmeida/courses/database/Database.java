package me.tomasalmeida.courses.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import me.tomasalmeida.courses.user.User;
import me.tomasalmeida.courses.user.UserDao;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.util.LinkedList;
import java.util.List;

public class Database {

    private MongoClient mongoClient;
    private Morphia morphia;
    private Datastore datastore;

    private static final String DATABASE = "kitpvp";
    private static Database instance;

    private UserDao userDao;

    private Database(){
        ServerAddress addr = createServerAddress("localhost", 27017);
        List<MongoCredential> credentials = getMongoCredentials("admin", "123");

        mongoClient = new MongoClient(addr, credentials);
        morphia = new Morphia();
        morphia.map(User.class);

        datastore = morphia.createDatastore(mongoClient, DATABASE);
        datastore.ensureIndexes();

        createDaos();
    }

    private void createDaos() {

        userDao = new UserDao(User.class, datastore);
    }

    public UserDao getUserDao() {
        return userDao;
    }

    private List<MongoCredential> getMongoCredentials(String username, String password) {
        List<MongoCredential> credentials = new LinkedList<>();
        credentials.add(MongoCredential.createCredential(username, DATABASE, password.toCharArray()));
        return credentials;
    }

    private ServerAddress createServerAddress(String hostname, int port){
        return new ServerAddress(hostname, port);
    }

    public static Database getInstance(){
        if(instance == null){
            synchronized (Database.class){
                if(instance == null){
                    instance = new Database();
                }
            }
        }

        return instance;
    }
}

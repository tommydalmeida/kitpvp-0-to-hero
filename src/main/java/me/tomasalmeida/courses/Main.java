package me.tomasalmeida.courses;

import me.tomasalmeida.courses.listeners.player.EntranceListener;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public void onEnable(){
        registerListeners(
                new EntranceListener()
        );
    }

    public void onDisable(){
        //TODO: disable database
    }

    private void registerListeners(Listener... listeners){
        for(Listener listener : listeners){
            Bukkit.getPluginManager().registerEvents(listener, this);
        }
    }
}
